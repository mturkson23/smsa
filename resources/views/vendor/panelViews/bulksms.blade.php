@extends('panelViews::mainTemplate')
@section('page-wrapper')

@if($callback) <div class="alert alert-info" role="alert">{!! $callback !!}</div> @endif
<br><br>
<p class="well">
Please select any of your previously saved messages (check the <b>Manage Messages</b> section)
and provide the appropriate Identifier name (in the <b>Sender ID</b> input) for your clients to know
who you are and click send.
</p>
<br>

	<div class="row">
		<div class="col-xs-6">
			{!! Form::open(array('route'=>'bulksms.form', 'method'=>'post')) !!}
				{!! Form::label('subject', 'Message Subject') !!}
				{!! Form::select('subject', $subjects, array('class' => 'form-control')) !!}
				<br />
				<!-- sender id -->
				{!! Form::label('sender_id', 'Sender ID') !!} &nbsp; <em style="color:blue;">(The name you want your clients to identify you with. Should not exceed eleven characters.)</em>
				{!! Form::text('sender_id', '', array('class' => 'form-control')) !!}
				<br />
				{!! Form::label('group', 'Recipient Group') !!}
				{!! Form::select('group', $groups, array('class' => 'form-control')) !!}
				<br/><br/>
				<!-- send message -->
				{!! Form::submit('Send Message', array('class' => 'btn btn-primary')) !!}

				{!! Form::close() !!}
			@stop
		</div>
	</div>
