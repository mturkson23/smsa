<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class GroupController extends CrudController{

    public function all($entity){
        parent::all($entity);
        /** Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields */

  			$this->filter = \DataFilter::source(new \App\Group);
  			$this->filter->add('recname', 'Group Name', 'text');
  			$this->filter->submit('search');
  			$this->filter->reset('reset');
  			$this->filter->build();

  			$this->grid = \DataGrid::source($this->filter);
  			$this->grid->add('recid', 'Group Number');
  			$this->grid->add('recname', 'Group Name');
  			$this->addStylesToGrid();

        return $this->returnView();
    }

    public function  edit($entity){
        parent::edit($entity);
        /* Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields */

    		$this->edit = \DataEdit::source(new \App\Group());
    		$this->edit->label('Edit Group Information');
    		$this->edit->add('recid', 'Group Number', 'text')->rule('required');
    		$this->edit->add('recname', 'Group Name', 'textarea')->rule('required');

        return $this->returnEditView();
    }
}
