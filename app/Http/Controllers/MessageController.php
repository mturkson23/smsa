<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class MessageController extends CrudController{

    public function all($entity){
        parent::all($entity);

        /** Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields */


			$this->filter = \DataFilter::source(new \App\Message);
			$this->filter->add('subject', 'Subject', 'text');
			$this->filter->submit('search');
			$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('subject', 'Subject');
			$this->grid->add('message', 'Message');
			$this->addStylesToGrid();

        return $this->returnView();
    }

    public function  edit($entity){

        parent::edit($entity);

        /* Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields */

			$this->edit = \DataEdit::source(new \App\Message());
			$this->edit->label('Edit Message');
			$this->edit->add('subject', 'Subject', 'text')->rule('required');
			$this->edit->add('message', 'Message', 'redactor')->rule('required');

        return $this->returnEditView();
    }
}
