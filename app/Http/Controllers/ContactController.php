<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class ContactController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        /** Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields */


			$this->filter = \DataFilter::source(new \App\Contact());
			$this->filter->add('first_name', 'First Name', 'text');
			$this->filter->add('surname', 'Surname', 'text');
			$this->filter->submit('search');
			$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('first_name', 'First Name', 'first_name');
			$this->grid->add('surname', 'Surname', 'surname');
			$this->grid->add('contact', 'Contact');
			$this->addStylesToGrid();
                 
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);
        /* Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields */
			$this->edit = \DataEdit::source(new \App\Contact());
			$this->edit->label('Edit Contact');
			$this->edit->add('first_name','First Name','text');		
			$this->edit->add('surname','Surname','text')->rule('required');
			$this->edit->add('contact','Contact','text')->rule('required');
       
        return $this->returnEditView();
    }    
}
