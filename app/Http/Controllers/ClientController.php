<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class ClientController extends CrudController{

    public function all($entity){
      parent::all($entity);
			$this->filter = \DataFilter::source(new \App\Client());
			//$this->filter->add('paid', 'Payment Status', 'text');
      //$this->filter->add('paid', 'Public', 'checkbox');
      $this->filter->add('paid','Payment Status','select')->options(\App\Paymentstatus::lists("recname","recid")->all());
			$this->filter->submit('search');
			//$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
      $this->grid->add('policynumber', 'Policy Number');
      $this->grid->add('fullname', 'Full Name');
			$this->grid->add('telephonenumber', 'Telephone Number');
			//$this->grid->add('dateofbirth', 'Date Of Birth');
			$this->grid->add('agentname', 'Agent Name');
      //$this->grid->add('paid', 'Payment Status');
      //$this->grid->add('groupid', 'Group Number');
			$this->addStylesToGrid();
      return $this->returnView();
    }

    public function  edit($entity){
      parent::edit($entity);
			$this->edit = \DataEdit::source(new \App\Client());
			$this->edit->label('Edit Client Details');
      $this->edit->add('policynumber', 'Policy Number', 'text')->rule('required');
      $this->edit->add('fullname', 'Full Name', 'text')->rule('required');
      $this->edit->add('telephonenumber', 'Telephone Number', 'text')->rule('required');
      $this->edit->add('dateofbirth', 'Date Of Birth', 'text')->rule('required');
      $this->edit->add('agentname', 'Agent Name', 'text')->rule('required');
      $this->edit->add('paid','Payment Status','select')->options(\App\Paymentstatus::lists("recname","recid")->all());
      $this->edit->add('groupid', 'Group Number', 'select')->options(\App\Group::lists("recname","recid")->all());
      return $this->returnEditView();
    }
}
