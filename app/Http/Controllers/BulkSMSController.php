<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use DB;

use Illuminate\Http\Request;

class BulkSMSController extends Controller{

	public function index(){

		$subjects = DB::table('messages')->lists('subject', 'id');
		$messages = DB::table('messages')->lists('message', 'id');
		$groups = DB::table('groups')->lists('recname', 'recid');

		$callback = false;
		return view('vendor.panelViews.bulksms')->with('subjects', $subjects)
					 																	->with('messages', $messages)
					 																  ->with('groups', $groups)
					 																	->with('callback', $callback);
	}

	public function sendMessage(Request $request){
		$API_KEY = "803dc6e12ae7c912eca5";
		// collect POST data from Form
		$message_data = array(
				'subject'   => $request->input('subject'),
				'sender_id' => $request->input('sender_id'),
				'group' => $request->input('group'),
		);
		// just a bit of a hack for now
		$subjects = DB::table('messages')->lists('subject', 'id');
		$messages = DB::table('messages')->lists('message', 'id');
		$groups = DB::table('groups')->lists('recname', 'recid');
		// query for the appropriate data to be sent
		// get subject from the id POSTed
		$subject = DB::table('messages')->where('id', $message_data['subject'])->lists('subject');
		// now the actual message
		$message = DB::table('messages')->where('id', $message_data['subject'])->lists('message');
		// format sender_id
		$sender_id = urlencode($message_data['sender_id']);
		$clients = DB::table('clients')->where('groupid',$message_data['group'])->get();
		// $clients = DB::table('clients')->get();
		if(! count($clients) > 0){

			$callback = "The group you selected has not been assigned any contacts yet";
			return view('vendor.panelViews.bulksms')->with('subjects', $subjects)
																							->with('messages', $messages)
																							->with('groups', $groups)
																							->with('callback', $callback);
		}
		foreach ($clients as $client){
			// replace the placeholders in the message
			$repl = array(
				"{TITLE}" => $client->title,
				"{POLICY_NUMBER}" => $client->policynumber,
				"{FULL_NAME}" => $client->fullname,
				"{TELEPHONE_NUMBER}" => $client->telephonenumber,
				"{AGENT_NAME}" => $client->agentname
			);
			$encoded_message = strtr($message[0], $repl);
			// urlencode message according to API
			$encoded_message = urlencode($encoded_message);
			$safe_contact = urlencode($client->telephonenumber);

			$API_URL = "http://bulk.mnotification.com/smsapi?key=$API_KEY&to=$safe_contact&msg=$encoded_message&sender_id=$sender_id";
			$result = file_get_contents($API_URL);
			header ("Content-Type:text/html");
		}

		$result = preg_replace("/[^0-9]/","",$result);
		switch($result){
			case "1000":
				$callback = "Message sent";
				break;
			case "1002":
				$callback = "Message not sent";
				break;
			case "1003":
				$callback = "Insufficient balance";
				break;
			case "1004":
				$callback = "Invalid API Key";
				break;
			case "1005":
				$callback = "Invalid Destination";
				break;
			case "1006":
				$callback = "Invalid Sender ID";
				break;
			case "1008":
				$callback = "Empty message";
				break;
			default:
				$callback = "Something went wrong. Please contact the service Administrator.";
				break;
		}
		//return $callback;
		return view('vendor.panelViews.bulksms')->with('subjects', $subjects)
																						->with('messages', $messages)
																						->with('groups', $groups)
																						->with('callback', $callback);
	}
}
