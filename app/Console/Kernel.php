<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();

        $schedule->call(function(){
          $dates = DB::table('clients')->whereRaw('MONTH(dateofbirth) = MONTH(NOW()) AND DAY(dateofbirth) = DAY(NOW())')->get();
      		$bdaymsg = DB::table('messages')->where('subject', ['Birthday'])->lists('message');
          if(count($dates)>0){
            foreach($dates as $date){
                DB::table('clients')
                    ->where('id', 1)
                    ->update(['status' => 1]);
            }
          }

        })->everyMinute();
    }
}
